import ast
import json
from config.service import relatar

def main():    
    print("Este es el comentario de un json cargado de un archivo")
    with open('archivo_json.json') as json_file: 
        data = json.load(json_file)
        comentarios = relatar(data)

    for comentario in comentarios:
        print(comentario)
    

    entrada = input("Introduzca una json de pelea por teclado:")
    json_pelea = ast.literal_eval(entrada)    
    comentarios = relatar(json_pelea)
    
    for comentario in comentarios:
        print(comentario)

if __name__ == '__main__':
    main()