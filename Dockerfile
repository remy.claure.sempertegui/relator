# pull official base image
FROM python:3.8-alpine3.10

# set work directory
WORKDIR /code
COPY . /code/	
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH