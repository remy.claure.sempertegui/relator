
class Jugador():
    nombre = None
    vidas = None
    combinaciones = None

    def __init__(self, nombre, combinacion, movimiento):
    # Inicio del constructor con vidas igual a 6
    # Parametros de inicio
    #    Nombre      : Nombre del jugador
    #    Combinacion : secuencia de movimientos y golpe asignada al jugador
    #    Movimiento  : para identificar si avanza o solo se mueve dependiendo de su posicion derecho o izquierdo

       self.vidas=6
       self.nombre = nombre
       self.combinaciones = combinacion
       self.movimientos = movimiento

    def get_jugada(self, jugada):
        if len(jugada['movimientos'])>0:
           movimiento=jugada['movimientos'].pop(0)           
        else:
             movimiento=''
        if len(jugada['golpes'])>0:
            golpe = jugada['golpes'].pop(0)
        else:
            golpe = ''
        return movimiento, golpe, jugada

    def ataque(self, movimiento, golpe, jugadorx):
    # Evaular movimiento y daño al oponente
        energia = 0
        combinatoria=''
        movi1 = ''
        movi2 = ''
        if movimiento[-3:]+'+'+golpe in self.combinaciones:            
            energia=self.combinaciones[movimiento[-3:]+'+'+golpe]['energia']
            combinatoria= ' conecta un '+self.combinaciones[movimiento[-3:]+'+'+golpe]['nombre_mov'] 
            movimiento=movimiento[:-3]
            golpe=''

        if movimiento[-2:]+'+'+golpe in self.combinaciones:            
            energia=self.combinaciones[movimiento[-2:]+'+'+golpe]['energia']
            combinatoria= ' conecta un '+self.combinaciones[movimiento[-2:]+'+'+golpe]['nombre_mov'] 
            movimiento=movimiento[:-2]
            golpe=''

        if golpe in self.combinaciones: 
           movi1=' da ' + self.combinaciones[golpe]['nombre_mov'] 
           energia+=self.combinaciones[golpe]['energia']

        
        if len(movimiento)>0:
            if movimiento in self.movimientos:
               movi2 = ' '+self.movimientos[movimiento]
            else:           
                movi2 = ' se mueve' 
            if len(movi1)>0:
               movi2 = movi2 + ' y le'
        if len(movi2 + combinatoria + movi1)>0:
           movimientos = self.nombre.split(' ')[0] + movi2 + combinatoria + movi1 
        else:
            movimientos = self.nombre.split(' ')[0] + " no se mueve"
        
        return movimientos, energia
  

def existe_ganador(vidas1, vidas2):
    return vidas1==0 or vidas2==0

def relatar(jugadas):
    #Inicializar jugadores con sus nombres y combinaciones especiales tambien las teclas que definen si avanza o retrocede dependiendo de la posicion jugador1 siempre a la izaquierda
    comentarios=[]
    jugador1 = Jugador('Tonnyn Stalone', {
               'DSD+P':{'nombre_mov': 'Taladoken', 'energia':3}, 
               'SD+K':{'nombre_mov': 'Remuyuken', 'energia':2}, 
               'P'   :{'nombre_mov': 'un puñetazo', 'energia':1}, 
               'K'   :{'nombre_mov': 'una patada', 'energia':1}
               },
               {
                'A'  : 'retrocede', #varia para el jugador 1 porque esta en la posicion izquierda "A" es para retroceder
                'D'  :'avanza' #varia para el jugador 1 porque esta a la izquierda "D" es para avanzar
                })
    jugador2 = Jugador('Arnaldor Shuatseneguer', {
               'SA+K':{'nombre_mov': 'Remuyuken', 'energia':3}, 
               'ASA+P':{'nombre_mov': 'Taladoken', 'energia':2}, 
               'P':{'nombre_mov': 'un puñetazo', 'energia':1}, 
               'K':{'nombre_mov': 'una patada', 'energia':1}
               },
               {
                'A'  : 'avanza', #varia para el jugador 1 porque esta en la posicion derecha "A" es para avanzar
                'D'  : 'retrocede' #varia para el jugador 1 porque esta a la derecha "D" es para retroceder
                })    
    try:
       jugada1 = jugadas['player1']
       jugada2 = jugadas['player2']

       total_mov_p1 = len(jugada1['movimientos'])+len(jugada1['golpes'])
       total_mov_p2 = len(jugada2['movimientos'])+len(jugada2['golpes'])
       if total_mov_p1 == total_mov_p2: # Si son iguales la candidad de movimientos mas golpes 
          total_mov_p1 = len(jugada1['movimientos'])
          total_mov_p2 = len(jugada2['movimientos'])
          if total_mov_p1 == total_mov_p2: # Si son iguales la candidad de movimientos 
             total_mov_p1 = len(jugada1['golpes'])
             total_mov_p2 = len(jugada2['golpes'])
             if total_mov_p1 == total_mov_p2: # Si son iguales la candidad de golpes empieza el jugador 1
                total_mov_p1 = 0
                total_mov_p2 = 1
       if total_mov_p1 < total_mov_p2:
          turno = 0
       else:
          turno = 1
       

       while (len(jugada1['movimientos'])+len(jugada2['movimientos'])+len(jugada1['golpes'])+len(jugada2['golpes'])>0) and not(existe_ganador(jugador1.vidas, jugador2.vidas)) and turno<19:  #El juego continua hasta que no existan jugadas y no exista un ganador
           if turno%2==0: # Si los turnos son pares es porque el jugador 1 empezo
              movimiento, golpe, jugada1 = jugador1.get_jugada(jugada1)            
              comentario, energia=jugador1.ataque(movimiento, golpe, jugador2)
              jugador2.vidas-=energia 
              if jugador2.vidas < 0:
                 jugador2.vidas=0             
           else: # Caso contrario el jugador 2 empezo
               movimiento, golpe, jugada2 = jugador2.get_jugada(jugada2)            
               comentario, energia=jugador2.ataque(movimiento, golpe, jugador1)
               jugador1.vidas-=energia
               if jugador1.vidas < 0:
                  jugador1.vidas=0             
           comentarios.append(comentario) #Se van apilando los comentarios
           if existe_ganador(jugador1.vidas, jugador2.vidas):
              if jugador1.vidas>jugador2.vidas:
                 ganador=jugador1.nombre.split(' ')[0]
                 vida=jugador1.vidas
              else:
                  ganador=jugador2.nombre.split(' ')[0]
                  vida=jugador2.vidas
              comentarios.append(ganador+' Gana la pelea y aun le queda '+str(vida)+' de energia')
           turno+=1 #siguiente turno       
    except Exception as inst:
        print(type(inst))    #Tipo de exception
        print(inst.args)     #Argumentos de la excepcion
    

    return comentarios