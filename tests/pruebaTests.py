import inspect, os, sys
current_dir = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())
))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

#from timeout_decorator import timeout
import unittest
from config.service import relatar


class RelatarTest(unittest.TestCase):
    """
    Los resultados de estas pruebas no afectaran a su score final
    """

    failureException = Exception

    def test_basic1(self):
        """
        Pelean en Json {"player1":{"movimientos":["D","DSD","S","DSD","SD"],"golpes":["K","P","","K","P"]},"player2": {"movimientos":["SA","SA","SA","ASA","SA"],"golpes":["K","","K","P","P"]}} 
        """
        json_pelea1 = {"player1":{"movimientos":["D","DSD","S","DSD","SD"],"golpes":["K","P","","K","P"]},"player2": {"movimientos":["SA","SA","SA","ASA","SA"],"golpes":["K","","K","P","P"]}}        
        comentarios1 = ['Tonnyn avanza y le da una patada','Arnaldor conecta un Remuyuken', 
                        'Tonnyn conecta un Taladoken','Arnaldor se mueve','Tonnyn se mueve','Arnaldor conecta un Remuyuken','Arnaldor Gana la pelea y aun le queda 2 de energia']
        self.assertEqual(relatar(json_pelea1), comentarios1)
    def test_basic2(self):
        json_pelea2 = {"player1":{"movimientos":["SDD", "DSD", "SA", "DSD"] ,"golpes":["K", "P", "K", "P"]}, "player2":{"movimientos":["DSD", "WSAW", "ASA", "", "ASA", "SA"],"golpes":["P", "K", "K", "K", "P", "k"]}}         
        comentarios2 = ["Tonnyn se mueve y le da una patada", "Arnaldor se mueve y le da un puñetazo", "Tonnyn conecta un Taladoken", "Arnaldor se mueve y le da una patada", "Tonnyn se mueve y le da una patada",
                        "Arnaldor avanza conecta un Remuyuken", "Tonnyn conecta un Taladoken", "Tonnyn Gana la pelea y aun le queda 1 de energia"]
        self.assertEqual(relatar(json_pelea2), comentarios2)
    def test_basic3(self):
        json_pelea3 = {"player1":{"movimientos":["DSD", "S"] ,"golpes":[ "P", ""]}, "player2":{"movimientos":["", "ASA", "DA", "AAA", "", "SA"],"golpes":["P", "", "P", "K", "K", "K"]}}  
        comentarios3 = ["Tonnyn conecta un Taladoken", "Arnaldor da un puñetazo", "Tonnyn se mueve", "Arnaldor se mueve", "Tonnyn no se mueve", "Arnaldor se mueve y le da un puñetazo",
                        "Tonnyn no se mueve", "Arnaldor se mueve y le da una patada", "Tonnyn no se mueve", "Arnaldor da una patada", "Tonnyn no se mueve", "Arnaldor conecta un Remuyuken", 
                        "Arnaldor Gana la pelea y aun le queda 3 de energia"]
        self.assertEqual(relatar(json_pelea3), comentarios3)
        